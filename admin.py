from django.contrib import admin

from nextlabapp.models import User
class UserAdmin(admin.ModelAdmin):
    list_dispaly = ('first_name','last_name','android_app','points_earned')

admin.site.register(User,UserAdmin)

# Register your models here.
