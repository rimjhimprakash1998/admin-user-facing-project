from django.apps import AppConfig


class NextlabappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nextlabapp'
